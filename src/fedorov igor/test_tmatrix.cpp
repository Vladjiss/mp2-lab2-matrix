#include "utmatrix.h"

#include <gtest.h>

TEST(TMatrix, can_create_matrix_with_positive_length)
{
  ASSERT_NO_THROW(TMatrix<int> m(5));
}

TEST(TMatrix, cant_create_too_large_matrix)
{
  ASSERT_ANY_THROW(TMatrix<int> m(MAX_MATRIX_SIZE + 1));
}

TEST(TMatrix, throws_when_create_matrix_with_negative_length)
{
  ASSERT_ANY_THROW(TMatrix<int> m(-5));
}

TEST(TMatrix, can_create_copied_matrix)
{
  TMatrix<int> m(5);

  ASSERT_NO_THROW(TMatrix<int> m1(m));
}

TEST(TMatrix, copied_matrix_is_equal_to_source_one)
{
	TMatrix<int> matr1(1), matr2(matr1);
	EXPECT_EQ(matr1, matr2);
}

TEST(TMatrix, copied_matrix_has_its_own_memory)
{
	TMatrix<int> matr1(1);
	matr1[0][0] = 1;
	TMatrix<int> matr2(matr1);
	matr1[0][0] = 0;
	EXPECT_EQ(1, matr2[0][0]);
}

TEST(TMatrix, can_get_size)
{
	TMatrix <int> matr(10);
	EXPECT_EQ(10, matr.GetSize());;
}

TEST(TMatrix, can_set_and_get_element)
{
	TMatrix<int> matr(10);
	matr[0][0] = 10;
	EXPECT_EQ(10, matr[0][0]);
	
}

TEST(TMatrix, throws_when_set_element_with_negative_index)
{
	TMatrix<int> matr(10);
	EXPECT_ANY_THROW(matr[-1][-1] = 10);
}

TEST(TMatrix, throws_when_set_element_with_too_large_index)
{
	TMatrix<int> matr(10);
	EXPECT_ANY_THROW(matr[100][100] = 2);
}

TEST(TMatrix, can_assign_matrix_to_itself)
{
	TMatrix<int> matr1(10);
	EXPECT_ANY_THROW(matr1=matr1);
}

TEST(TMatrix, can_assign_matrices_of_equal_size)
{
	TMatrix<int> matr1(10),matr2(10);
	EXPECT_NO_THROW(matr1 = matr2);
}

TEST(TMatrix, assign_operator_change_matrix_size)
{

}

TEST(TMatrix, can_assign_matrices_of_different_size)
{
	TMatrix<int> matr1(10),matr2(11);
	EXPECT_NO_THROW(matr1 = matr2);
}

TEST(TMatrix, compare_equal_matrices_return_true)
{
	TMatrix<int> matr1(10), matr2(10);
	EXPECT_TRUE(matr1 == matr2);
}

TEST(TMatrix, compare_matrix_with_itself_return_true)
{
	TMatrix<int> matr1(10);
	EXPECT_TRUE(matr1 == matr1);
}

TEST(TMatrix, matrices_with_different_size_are_not_equal)
{
	TMatrix<int> matr1(10), matr2(11);
	EXPECT_FALSE(matr1 == matr2);
}

TEST(TMatrix, can_add_matrices_with_equal_size)
{
	TMatrix<int> matr1(10), matr2(10), matr3(10);
	matr1[0][0] = 1;
	matr2[1][1] = 1;

	matr3[0][0] = 1;
	matr3[1][1] = 1;

	matr2 = matr2 + matr1;
	EXPECT_TRUE(matr3 == matr2);
}

TEST(TMatrix, cant_add_matrices_with_not_equal_size)
{
	TMatrix<int> matr1(10), matr2(11);
	EXPECT_ANY_THROW(matr1 = matr1 + matr2);
}

TEST(TMatrix, can_subtract_matrices_with_equal_size)
{	
	TMatrix<int> m1(10), m2(10), m3(10);
	for (int i = 0; i < 10; i++) {
		m1[i][i] = i;
		m2[i][i] = i;
		m3[i][i] = 0;
	}
	EXPECT_EQ(m1 - m2, m3);
}

TEST(TMatrix, cant_subtract_matrixes_with_not_equal_size)
{
	TMatrix<int> matr1(10), matr2(11);
	EXPECT_ANY_THROW(matr1 = matr1 - matr2);

}

